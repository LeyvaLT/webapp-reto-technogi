import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import Form from './components/Form';
import Card from './components/Card';
import { fetch } from './reducers/articulos';
import './App.css'


const App = ({fetchData, articulos, fetched}) => {
  useEffect(() => {
    fetchData();
    console.log();
  }, []);

  return (
    <div>
      <Form />
      <div className="container-cards">
        {fetched && (
          articulos.map(articulo => (
              <Card data={articulo} key={articulo._id}/>
          ))
          )}
      </div>
    </div>
  )
}


const mapStateToProps = (state) => ({
  articulos: state.articulos.data,
  fetched: state.articulos.fetched
});

const mapDispatchToProps = (dispatch) => ({
  fetchData: () => dispatch(fetch()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

