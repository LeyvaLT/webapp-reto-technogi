import CRUD from './creators/crud';

export const {
  reducer: articulos, fetch, remove, post
} = CRUD('articulos');
