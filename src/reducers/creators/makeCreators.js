// make type
const makeType = (modulo) => ({
  FETCH_SUCCESS: `${modulo}/FETCH_SUCCESS`,
  FETCH_ERROR: `${modulo}/FETCH_ERROR`,
  FETCH_START: `${modulo}/FETCH_START`,
  POST_SUCCESS: `${modulo}/POST_SUCCESS`,
  POST_ERROR: `${modulo}/POST_ERROR`,
  POST_START: `${modulo}/POST_START`,
  DELETE_SUCCESS: `${modulo}/DELETE_SUCCESS`,
  DELETE_ERROR: `${modulo}/DELETE_ERROR`,
  DELETE_START: `${modulo}/DELETE_START`,
});

// make action creator
const makeActionCreator = (type, ...argNames) => (...args) => {
  const action = { type };
  argNames.forEach((arg, index) => {
    action[argNames[index]] = args[index];
  });
  return action;
};

// make reducer
const makeReducer = (initialState, handlers = {}) => (state = initialState, action) => {
  if (!Object.prototype.hasOwnProperty.call(handlers, action.type)) return state;
  return handlers[action.type](state, action);
};

export {
  makeType,
  makeActionCreator,
  makeReducer,
};
