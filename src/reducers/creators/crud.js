import axios from '../../config/axios';
import { makeType, makeActionCreator, makeReducer } from './makeCreators';

const CRUD = (resource) => {
	const {
		FETCH_SUCCESS,
		FETCH_ERROR,
		FETCH_START,
		POST_SUCCESS,
		POST_ERROR,
		POST_START,
		DELETE_SUCCESS,
		DELETE_ERROR,
		DELETE_START,
	} = makeType(resource);

	const initialState = {
		data: [],
		fetched: false,
		fetching: false,
		error: '',
	};

	// Get Data

	const fetchStart = makeActionCreator(FETCH_START);
	const fetchSuccess = makeActionCreator(FETCH_SUCCESS, 'payload');
	const fetchError = makeActionCreator(FETCH_ERROR, 'error');

	const fetchStartReduce = (state = initialState) => ({ ...state, fetching: true });
	const fetchErrorReduce = (state = initialState, action) => ({ ...state, fetching: false, error: action.error });
	const fetchSuccessReduce = (state = initialState, action) => ({
		...state,
		fetching: false,
		fetched: true,
		error: '',
		data: action.payload,
	});

	//

	// POST Data
	const postStart = makeActionCreator(POST_START);
	const postSuccess = makeActionCreator(POST_SUCCESS, 'payload');
	const postError = makeActionCreator(POST_ERROR, 'error');

	const postStartReduce = (state = initialState) => ({ ...state, fetching: true, error: '' });
	const postErrorReduce = (state = initialState, action) => ({
		...state, fetching: false, error: action.error, postError: true
	});
	const postSuccessReduce = (state = initialState, action) => {
		action.payload.createdAt = (new Date()).toString();

		return ({
			...state,
			fetching: false,
			fetched: true,
			data: [...state.data, action.payload],
		});
		//
	};


	// Delete Data
	const deleteStart = makeActionCreator(DELETE_START);
	const deleteSuccess = makeActionCreator(DELETE_SUCCESS, 'payload');
	const deleteError = makeActionCreator(DELETE_ERROR, 'error');

	const deleteStartReduce = (state = initialState) => ({ ...state, fetching: true, error: '' });
	const deleteErrorReduce = (state = initialState, action) => ({ ...state, fetching: false, error: action.error });
	const deleteSuccessReduce = (state = initialState, action) => ({
		...state,
		fetching: false,
		fetched: true,
		filtered: false,
		data: state.data.filter((x) => x._id !== action.payload._id),
	});
	//

	// Reducer
	const reducer = makeReducer(initialState, {
		[FETCH_START]: fetchStartReduce,
		[FETCH_SUCCESS]: fetchSuccessReduce,
		[FETCH_ERROR]: fetchErrorReduce,
		[POST_START]: postStartReduce,
		[POST_SUCCESS]: postSuccessReduce,
		[POST_ERROR]: postErrorReduce,
		[DELETE_START]: deleteStartReduce,
		[DELETE_SUCCESS]: deleteSuccessReduce,
		[DELETE_ERROR]: deleteErrorReduce,
	});
	//

	return {
		reducer,
		fetch: () => async (dispatch) => {
			dispatch(fetchStart());
			try {
				const response = await axios().get(`${resource}/`);
				dispatch(fetchSuccess(response.data));
			} catch (error) {
				dispatch(fetchError(error));
			}
		},
		post: (data) => async (dispatch) => {
			dispatch(postStart());
			try {
				return axios().post(`${resource}/`, { ...data }).then((response) => {
						if (response.ok === false) {
							return dispatch(postError(`Error al dar de alta ${resource}`));
						}
						dispatch(postSuccess({ ...data, _id: response.data._id }));
					}).catch((error) => {
						dispatch(postError(error))
					});
			} catch (error) {
				dispatch(postError(`Error al dar de alta ${resource}: ${error}`));
			}
		},
		remove: (id) => (dispatch) => {
			dispatch(deleteStart());
			try {
				axios().delete(`${resource}/${id}/`)
					.then((response) => {
						if (!response) {
							return dispatch(deleteError(`error al eliminar ${resource}`));
						} 
						dispatch(deleteSuccess({ _id: id }));
					})
					.catch((error) => {
						dispatch(deleteError(`error al eliminar ${resource}: ${error}`));
					});
			} catch (error) {
				return dispatch(deleteError(error));
			}
		}
	};
};

export default CRUD;
