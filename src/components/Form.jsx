import React, { useState } from 'react';
import { Button, Form, Alert } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { post } from '../reducers/articulos';
import './styles/Form.css'

const Formulario = () => {
	const dispatch = useDispatch();
	const [error, setError] = useState(false);
	const [apiError, setApiError] = useState(false);
	const [apiSuccess, setApiSuccess] = useState(false);
	const [datos, setDatos] = useState({
		title: '',
		text: ''
	});

	const handleInputChange = (event) => {
		setApiSuccess(false);
		setApiError(false);
		setError(false);
		setDatos({
				...datos,
				[event.target.name] : event.target.value
		})
	}

	const handleCancel = () => {
		setDatos({
			title: '',
			text: ''
		});
	};
	const handleSubmit = (event) => {
		event.preventDefault();
		if (datos.title.length > 0 && datos.text.length > 0) {
			setError(false);
			setDatos({
				title: '',
				text: ''
			});
			dispatch(post(datos)).then(() => setApiSuccess(true))
				.catch(() => setApiError(true))
		} else setError(true);
	}
	return (
		<div className='form-container'>
			<Form.Group controlId="formBasicEmail">
				<Form.Label>Titulo</Form.Label>
				<Form.Control type="text" name="title" value={datos.title} onChange={handleInputChange} />
			</Form.Group>

			<Form.Group controlId="exampleForm.ControlTextarea1">
				<Form.Label>Contenido</Form.Label>
				<Form.Control as="textarea" rows={3} name="text" value={datos.text}  onChange={handleInputChange} />
			</Form.Group>
			<div className="row-btn">
				<Button variant="light" type="submit" onClick={handleCancel} className='btn'>
					Cancelar
				</Button>
				<Button variant="primary" type="submit" onClick={handleSubmit}>
					Guardar
				</Button>
			</div>
			{error && (
				<Alert variant="danger">
					Debes de llenar todos los campos del Formulario
				</Alert>
			)}
			{apiSuccess && (
				<Alert variant="success">
					El articulo se inserto correctamente.
				</Alert>
			)}
			{apiError && (
				<Alert variant="danger">
					Error interno del servidor
				</Alert>
			)}
		</div>
	)
};

export default Formulario;
