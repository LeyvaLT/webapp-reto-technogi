import React from 'react';
import { Button, Card } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { remove } from '../reducers/articulos';
import './styles/Card.css'

const Cards = ({ data }) => {
	const dispatch = useDispatch();
	const handleDelete = (event) => {
		event.preventDefault();
		dispatch(remove(data._id));
	}
	return (
		<Card className='card'>
			<Card.Body>
				<Card.Title>{data.title}</Card.Title>
				<Card.Text>{data.text}</Card.Text>
				<div className='card-btn'>
					<Button variant="danger" onClick={handleDelete}>Borrar</Button>
				</div>
			</Card.Body>
		</Card>
	)
}

export default Cards;
