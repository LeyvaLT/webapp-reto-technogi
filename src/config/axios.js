import axios from 'axios';
import { BASE_URL } from './constants';

const axiosInstance = () => {
  const config = axios.create();
  config.defaults.baseURL = BASE_URL;
  config.interceptors.response.use((res) => res.data,
    (error) => Promise.reject(error.response.data));
  return config;
};

export default axiosInstance;
